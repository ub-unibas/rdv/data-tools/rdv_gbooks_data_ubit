import re
from collections import Counter

from rdv_data_helpers_ubit import IIIF_MANIF_ESFIELD, OLDSYS_ESFIELD
from rdv_data_helpers_ubit.projects.hierarchy.hierarchy import HIERARCHY_FIELD
from rdv_hierarchy_ubit import MarcDefHierachy
from rdv_oaimarc_data_ubit import RDVOAIMarcData
from rdv_entity_data_ubit import RDVEntityData

class RDVGBBibEntity(RDVEntityData):

    @property
    def link_field(self):
        """field which is used also in Object-Index to create Links"""
        return "adress_empf"

    @property
    def entity_type(self):
        return "AutorIn"

class RDVGBBib(RDVOAIMarcData):
    index = "gb"
    pers_index = "gbbibpers"
    pers_index_prefix = "gbbibpers"
    transkription = False
    entity_class = RDVGBBibEntity

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.project_spreadsheet = ""
        self.marc_hierarchy = MarcDefHierachy(host="localhost:9209",
                                              index="marcdef",
                                              index_prefix="marcdef",
                                              logger=kwargs["logger"])
        self.ubs_second = {line.strip().replace("(IDSBB)", "1_").replace("DSV01", ""): 1
                           for line in open("/home/martin/PycharmProjects/rdv_gbbib_data_ubit/rdv_gbbib_data_ubit/gb_ubs_second.csv")}
        self.missing_hier_fields = Counter()

    def get_entities_data(self, index_prefix):
        entities_data = {}
        count_persons = len(self.entities)
        for n, person in enumerate(self.entities.values()):
            if n % 100:
                print("{} von {} Personen angereichert".format(n, count_persons))
            entity_object = self.entity_class(entity=person,
                                              entity_object_lookup=self.object_entity_lookup,
                                              object_link_prefix="de/detail/{}_{{}}".format(index_prefix),
                                              cachedecorator=self.entity_cachedecorator,
                                              logger=self.logger)
            gnd_id, entity_data = entity_object.get_gnd_data()
            entities_data[gnd_id] = entity_data
        return entities_data

    def extend_record_data(self, record, bsiz_id):
        record["type"] = "Objekt"
        record[HIERARCHY_FIELD] = []
        for marc_hier_id in record["record_marchier_id"]:

            if self.marc_hierarchy.es_thesaurus.get(marc_hier_id):
                record.setdefault(HIERARCHY_FIELD, []).extend(self.marc_hierarchy.es_thesaurus.get(marc_hier_id,{}).get(HIERARCHY_FIELD))
            else:
                self.missing_hier_fields[marc_hier_id] += 1
        old_sys = record.get(OLDSYS_ESFIELD,"")

        if old_sys in self.ubs_second:
            record["lib"] = ["UB Basel"]
        else:
            record["lib"] = ["UB Basel", "ZB Zürich"]

        for s in record.get("signatur",[]):
            record.setdefault("split_signatur",[]).append(s)
            s_begin = ""
            for n, s_p in enumerate(s.split(" ")):
                s_begin += " " + s_p if n != 0 else s_p
                if n != 0 and n!= len(s.split(" ")) and s_begin:
                    record["split_signatur"].append(s_begin)

        for e in record.get("581_a",[]):
            e = re.split(", S\. \d{1}", e)
            record.setdefault("581_a_norm", []).append(e[0])

        for e in record.get("510_a",[]):
            e = re.split(", S\. \d{1}", e)
            record.setdefault("510_a_norm", []).append(e[0])

        for e in record.get("300_a",[]):
            e = re.sub("\d{1}", "#", e)
            record.setdefault("300_a_norm", []).append(e)

        for e in record.get("300_c",[]):
            e = re.sub("\d{1}", "#", e)
            record.setdefault("300_c_norm", []).append(e)

        record.setdefault("655_a_7", []).extend(record.get("336_b",[]))

        manif = record.get(IIIF_MANIF_ESFIELD)
        if manif:
            vlm_id = manif[0].split("/")[-2]
            record["vlm_id"] = vlm_id

        for person in record.get("author", []):
            id_ = person.get("id")
            self.entities[id_] = person
            if id_:
                self.object_entity_lookup.setdefault(person.get("id") or person.get("label"), []).append(record)
            record.setdefault("adress_empf",[]).append(person)
        for e in record.get("rel_persons", []):
            role = e.get("role")
            if role and set([role]) & set(["prt", "Drucker"]):
                record.setdefault("Drucker",[]).append(e)



